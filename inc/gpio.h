#ifndef GPIO_H
#define GPIO_H

#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>

int sensor[8];
int gpio_pin[6];

void turn_on(int);
void turn_off(int);
void setup_gpio();
void sensor1();
void sensor2();
void sensor3();
void sensor4();
void sensor5();
void sensor6();
void sensor7();
void sensor8();
void event_handler();

#endif 
