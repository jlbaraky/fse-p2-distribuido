#ifndef __BME280__
#define __BME280__

int bme280Init(int iChannel, int iAddr);
int bme280ReadValues(int *T, int *P, int *H);
void get_bme280_values(float *temperature, float *humidity);
void send_bme280_values();

#endif // __BME280__
