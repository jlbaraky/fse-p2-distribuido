# Projeto 2 - 2020/2 - Distribuído

## Compilação

Para compilar, digite o comando abaixo dentro do diretório do projeto:

`$ make`

## Execução

Para executar o programa, digite o comando abaixo:

`$ make run` 

## Utilização

O ambiente distribuído não possui interação com o usuário, porém é **necessário** subir o servidor **apenas** depois do central.
