#include "gpio.h"
#include "server.h"

static volatile int state[8];

void turn_on(int pin) {
    digitalWrite(pin, HIGH);
    delay(10);
}

void turn_off(int pin) {
    digitalWrite(pin, LOW);
    delay(10);
}

void setup_gpio() {
    sensor[0] = 6, sensor[1] = 25, sensor[2] = 21, sensor[3] = 22,
	    sensor[4] = 26, sensor[5] = 27, sensor[6] = 28, sensor[7] = 29;
    gpio_pin[0] = 0, gpio_pin[1] = 1, gpio_pin[2] = 2,
	    gpio_pin[3] = 3, gpio_pin[4] = 23, gpio_pin[5] = 24;

    if (wiringPiSetup() == -1) 
        exit(1);

    for (int i = 0; i < 6; i++) {
        pinMode(gpio_pin[i], OUTPUT);
    }

    for (int i = 0; i < 8; i++) {
        pinMode(sensor[i], OUTPUT);
    }

    wiringPiISR(sensor[0], INT_EDGE_BOTH, &sensor1);
    wiringPiISR(sensor[1], INT_EDGE_BOTH, &sensor2);
    wiringPiISR(sensor[2], INT_EDGE_BOTH, &sensor3);
    wiringPiISR(sensor[3], INT_EDGE_BOTH, &sensor4);
    wiringPiISR(sensor[4], INT_EDGE_BOTH, &sensor5);
    wiringPiISR(sensor[5], INT_EDGE_BOTH, &sensor6);
    wiringPiISR(sensor[6], INT_EDGE_BOTH, &sensor7);
    wiringPiISR(sensor[7], INT_EDGE_BOTH, &sensor8);
}

void sensor1() {send_message(SENSOR, 0);}
void sensor2() {send_message(SENSOR, 1);}
void sensor3() {send_message(SENSOR, 2);}
void sensor4() {send_message(SENSOR, 3);}
void sensor5() {send_message(SENSOR, 4);}
void sensor6() {send_message(SENSOR, 5);}
void sensor7() {send_message(SENSOR, 6);}
void sensor8() {send_message(SENSOR, 7);}

void event_handler() {
    for (int i = 0; i < 8; i++) {
        state[i] = digitalRead(sensor[i]);
	if (state[i])
	    send_message(SENSOR, i);
    }

    while(1) sleep(1);
}
