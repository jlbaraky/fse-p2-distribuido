#include "server.h"
#include "gpio.h"

void message_handler(int pin, int state) {
    pin = gpio_pin[pin-1];

    if (state == 1)
        turn_on(pin);
    else
        turn_off(pin);
}

void send_message(int type, float obj) {
    char message[6];
    connect_to_central();

    message[0] = type + '0';
    memcpy(&message[1], &obj, 4);
    message[5] = '\0';

    if(send(clientSocket, message, 6, 0) != 6)
        printf("Erro no envio: numero de bytes enviados diferente do esperado\n");

    close(clientSocket);
}

void client_handler(int clientSocket) {
	char buffer[3];
	int rxLength;
        int pin, state;

	if((rxLength = recv(clientSocket, buffer, 3, 0)) < 0)
		printf("Erro no recv()\n");

	pin = buffer[0] - '0';
	state = buffer[1] - '0';
	message_handler(pin, state);

	while (rxLength > 0) {
		if(send(clientSocket, buffer, rxLength, 0) != rxLength)
			printf("Erro no envio - send()\n");
		
        if((rxLength = recv(clientSocket, buffer, 3, 0)) < 0)
			printf("Erro no recv()\n");
	}

}

void start_listening() {
    unsigned int clientLength;
	while(1) {
		clientLength = sizeof(clientAddr);
		if((clientSocket = accept(distServerSocket, 
			                      (struct sockaddr *) &clientAddr, 
			                      &clientLength)) < 0)
			printf("Falha no Accept\n");
		
		//printf("Conexão do Cliente %s\n", inet_ntoa(clientAddr.sin_addr));
		
		client_handler(clientSocket);
		close(clientSocket);
	}
}

void start_server() {
    setup_server();
    start_listening();
}

void connect_to_central() {
	if((clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");

    memset(&centralServerAddr, 0, sizeof(centralServerAddr));
	centralServerAddr.sin_family = AF_INET;
	centralServerAddr.sin_addr.s_addr = inet_addr("192.168.0.53");
	centralServerAddr.sin_port = htons(10032);
	if(connect(clientSocket, (struct sockaddr *) &centralServerAddr, 
								sizeof(centralServerAddr)) < 0)
		printf("Erro no connect()\n");
}

void setup_server() {
	if((distServerSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");

	memset(&distServerAddr, 0, sizeof(distServerAddr));
	distServerAddr.sin_family = AF_INET;
	distServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	distServerAddr.sin_port = htons(10132);

	if(bind(distServerSocket, (struct sockaddr *) &distServerAddr, sizeof(distServerAddr)) < 0)
		printf("Falha no Bind\n");

	if(listen(distServerSocket, 10) < 0)
		printf("Falha no Listen\n");		

}

void shut_down_server() {
    close(clientSocket);
    close(distServerSocket);
}
