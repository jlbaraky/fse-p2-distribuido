#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include "server.h"
#include "gpio.h"
#include "bme280.h"

void shut_down() {
    shut_down_server();
    exit(0);
}

int main() {
    pthread_t server, event;
    signal(SIGINT, shut_down);
    signal(SIGALRM, send_bme280_values);

    setup_gpio();
    pthread_create(&server, NULL, (void*)start_server, NULL);
    pthread_create(&event, NULL, (void*)event_handler, NULL);

    send_bme280_values();

    while(1) {sleep(1);}

    return 0;
}
